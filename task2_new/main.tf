#Terraform Block
terraform {
  required_version = "v0.14.10"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 3.36.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}



#EC2 Instance
resource "aws_instance" "EC2instance" {
  ami                    = "ami-0742b4e673072066f"
  instance_type          = "t2.micro"
  key_name               = "webserver"
  vpc_security_group_ids = [aws_security_group.Web.id]
  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y",
      "sudo yum install -y httpd httpd-tools mod_ssl",
      "sudo systemctl enable httpd",
      "sudo systemctl enable httpd",
      "sudo amazon-linux-extras enable php7.4",
      "sudo yum clean metadata",
      "sudo yum install php php-common php-pear -y",
      "sudo yum install -y php-{cgi,curl,mbstring,gd,mysqlnd,gettext,json,xml,fpm,intl,zop}",
      "sudo rpm -Uvh https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm",
      "sudo yum install mysql-community-server -y",
      "sudo systemctl enable mysqld",
      "sudo systemctl start mysqld",
      "sudo usermod -a -G apache ec2-user",
      "sudo chown -R ec2-user:apache /var/www",
      "sudo chmod 2775 /var/www && find /var/www -type d -exec sudo chmod 2775 {} \\;",
      "sudo find /var/www -type f -exec sudo chmod 0664 {} \\;",
      "wget https://wordpress.org/latest.tar.gz",
      "tar -xzf latest.tar.gz",
      "cp -r wordpress/* /var/www/html/",
      "sudo service httpd restart",

    ]
  }


  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ec2-user"
    private_key = file("webserver")

  }


  tags = {
    Name = "wordpressT"
  }
}

#KeyPair
resource "aws_key_pair" "owner" {
  key_name   = "webserver"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDZuApEIsYUyooLW2+/3ONAP0h1YT93UNptkyx1Zgus+OhTRjfeYrFNZhg0X1UnDQcrai0h2ZsQV1C4SQawGxBc86eyf3KZgrrCLh7InLwdlb17I+dmfrNc8i7UF1EXdytduu99vlXZ6LrPf5QLXUjLFzO3nBrtZH4SqFUJrEPnMhN+Wp+QnjiFmwbqKOF6ld+z1rjyx+3ET5lSSsaD2BqTMTf+8fzty1bVnUhJTWxvY+w0PXH+AhXeDQtGWLEmU9777bUdLcGhrNeytmWcTaKwj7C/nzBTsYVPbUdtO+DbSL78z/Gg589vV4qvvrlBkRHbhrGdPU+tRFq2LL3zoruDFXcW7+AHONCYJLBJhbGJCsy3PHp7aYULbwodf14HECS/YGwd+BWnjFJO3Qm7UNFHv4k+7SGoq8xhwTHIIEsd4JWZfO/eJrHQouMTca1ZZ2vWcz6m53ps1zC5z5WoXjq/JdT0Pa2MbOkfuxrwwsjw9glIcfzDIrY8C9Yhi6WIHrM= Prajwal@DESKTOP-J088C6I"
}

#Security Group
resource "aws_security_group" "Web" {
  name = "Web-SG"

  ingress {
    description = "Allow SSH inbound connection"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["157.33.111.2/32"]
  }
  ingress {
    description = "Allow HTTP request"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Allow HTTPS request"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description = "All Traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#RDS Instance
resource "aws_db_instance" "Database" {
  allocated_storage = 20
  #Storage autoscaling
  #max_allocated_storage = 100
  engine                 = "mysql"
  engine_version         = "8.0.20"
  instance_class         = "db.t2.micro"
  username               = "admin"
  password               = "admin12345"
  name                   = "wordpress"
  vpc_security_group_ids = [aws_security_group.RDS.id]

}

#security group

resource "aws_security_group" "RDS" {
  name = "RDS-SG"

  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.Web.id]
  }
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.Web.id]
  }

  egress {
    description = "All Traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

